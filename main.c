
#include <asf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/power.h>
#include <avr/sleep.h>

#define GPS_BAUD 9600
#define UART_UBRR (F_CPU/GPS_BAUD/16-1)

#define LCD_DATA0_PORT	PORTD
#define LCD_DATA1_PORT	PORTD
#define LCD_DATA2_PORT	PORTD
#define LCD_DATA3_PORT	PORTB

#define LCD_DATA0_PIN	7
#define LCD_DATA1_PIN	6
#define LCD_DATA2_PIN	5
#define LCD_DATA3_PIN	7

#define LCD_RS_PORT	PORTD
#define LCD_RS_PIN	4

#define LCD_RW_PORT	PORTD
#define LCD_RW_PIN	3

#define LCD_E_PORT	PORTB
#define LCD_E_PIN	6

#include "nmea.h"
#include "tools.h"
#include "sd/ff.h"
#include "menu.h"

static void init(void);
static void gps_uart_init(void);

FATFS fs;
FIL file;

pin LED = {B, 0};
pin GPS_E = {D, 2};

utc_date date = { 31, 7, 16 };

char filename[11];

btn_handler btn_handlers[6];

int main(void) {
	init();

	BLINK(LED.port, LED.pin, 100, 0, 1);

	FRESULT result = f_mount(&fs, "", 0);
	if (result != FR_OK)
		goto error;
	
	result = f_open(&file, filename, FA_WRITE | FA_OPEN_EXISTING);
	if (result != FR_OK) {
		result = f_open(&file, filename, FA_WRITE | FA_CREATE_ALWAYS);
		if (result != FR_OK)
		goto error;
	}
	
	ubyte size;
	ubyte kkk = 0;
	while(size > 1000000) {
		
	}
	
	io_high(GPS_E.port, GPS_E.pin);
	
	while (1) {
		PWM(LED.port, LED.pin, 20, 10);
		
		//set_sleep_mode(SLEEP_MODE_IDLE);
		//sleep_enable();
		//sleep_mode();
		
		_delay_ms(980);
	}

error:	while(1) {
		PWM(LED.port, LED.pin, 20, 10)
		_delay_ms(100);
	}
}

static void parseLetters(string into, ubyte number) {
	into[0] = number / 10 + '0';
	into[1] = number % 10 + '0';
}

static void init(void) {
	// power reduction
	PRR |= BIT(PRTWI) | BIT(PRTIM2) | BIT(PRTIM1) | BIT(PRTIM0) | BIT(PRADC);
	
	//configure filename
	parseLetters(&filename[0], date.year);
	parseLetters(&filename[2], date.month);
	parseLetters(&filename[4], date.day);
	filename[6] = '.';
	filename[7] = 't';
	filename[8] = 'x';
	filename[9] = 't';
	filename[10] = '\0';
	
	// init buttons
	btn_handlers[BTN_BACK]	= btn_back_onclick;
	btn_handlers[BTN_OK]	= btn_ok_onclick;
	btn_handlers[BTN_LEFT]	= btn_left_onclick;
	btn_handlers[BTN_RIGHT]	= btn_right_onclick;
	btn_handlers[BTN_DOWN]	= btn_down_onclick;
	btn_handlers[BTN_UP]	= btn_up_onclick;
	
	//configure pins
	io_dir(LED.port, LED.pin, Out);
	io_dir(GPS_E.port, GPS_E.pin, Out);
	io_low(GPS_E.port, GPS_E.pin);

	gps_uart_init();

	ENABLE_BUTTONS();
	sei();
}

static void gps_uart_init(void) {
	UBRR0L = (ubyte) UART_UBRR & 0xFF;
	UBRR0H = (ubyte) (UART_UBRR >> 8) & 0xFF;
	
	UCSR0C = BIT(UCSZ01) | BIT(UCSZ00);
	UCSR0B = BIT(RXCIE0) | BIT(RXEN0);
}

ubyte btn_history = 0x00;

ISR(PCINT1_vect) {
	DISABLE_BUTTONS();
	ubyte changed = btn_history ^ BUTTON_STATES();
	btn_history = BUTTON_STATES();
	if (btn_history == 0) return;
	if (changed) {
		btn_handlers[changed]();
	}
	_delay_ms(20);
	ENABLE_BUTTONS();
}

#define sentence_size 100
char sentence[sentence_size];
ubyte i;
nmea x;
typedef enum RxStatus { WaitForDollar, HasDollar, WaitForEnd } RxStatus;
RxStatus status = WaitForDollar;
DWORD file_size;

ISR(USART_RX_vect) {
	ubyte received = UART_DATA;
	switch(status) {
		case WaitForDollar:
			if (received == '$') {
				status = HasDollar;
				sentence[0] = received;
				i = 1;
				return;
			} else
				return;
		
		case HasDollar:
			if (received == '\n') {
				status = WaitForDollar;
				if (i > 1) {
					sentence[i] = '\0';
					sentence[i - 1] = '\n';
					bool success = parse_nmea(sentence, &x);
					if (x.kind == GGA) {
						UINT bytesWritten;
						FRESULT result = f_lseek(&file, file.fsize);
						result = f_write(&file, sentence, i, &bytesWritten);
						result = f_sync(&file);
						x.kind = 255;
						//PWM(LED.port, LED.pin, 20, 50);
					}
				}
				return;
			} else {
				sentence[i] = received;
				i++;
				return;
			}
	}
}

#undef sentence_size
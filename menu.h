/*
 * menu.h
 *
 * Created: 2016-08-01 02:24:59
 *  Author: Paluszki
 */ 


#ifndef MENU_H_
#define MENU_H_

#define BTN_OK		3
#define BTN_BACK	0
#define BTN_LEFT	2
#define BTN_RIGHT	4
#define BTN_DOWN	1
#define BTN_UP		5

#define ENABLE_BUTTONS()	r_set(PCMSK1, 0x3f); r_set(PCICR, BIT(PCIE1))
#define DISABLE_BUTTONS()	r_clear(PCICR, BIT(PCIE1)); r_clear(PCMSK1, 0x3f);
#define BUTTON_STATES()		(~PORT_READ(C) & 0x3f)
#define BUTTON_STATE(i)		(BUTTON_STATES() & BIT(i))

typedef void (*btn_handler)(void);

void btn_ok_onclick(void);
void btn_back_onclick(void);
void btn_left_onclick(void);
void btn_right_onclick(void);
void btn_down_onclick(void);
void btn_up_onclick(void);


#endif /* MENU_H_ */
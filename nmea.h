
#ifndef NMEA_H_
#define NMEA_H_

#include "tools.h"

enum NmeaKind { GGA, GSA, GSV, RMC };
typedef enum NmeaKind NmeaKind;

enum NmeaFix { NoFix = 0, GPS = 1, Differential = 2 };
typedef enum NmeaFix NmeaFix;

typedef struct utc_time {
	ubyte	hours;
	ubyte	minutes;
	ubyte	seconds;
	ushort	milis;
} utc_time;

typedef struct utc_date {
	ubyte	day;
	ubyte	month;
	ubyte	year;
} utc_date;

typedef struct value {
	float	value;
	char	unit;
} value;

typedef struct nmea {
	NmeaKind kind;
	void* data;
} nmea;

typedef struct nmea_gga {
	utc_time	time;
	uint32_t	latitude;	// deg * 10000
	uint32_t	longitude;	// deg * 10000
	NmeaFix		fix;
	ubyte		sattelites;
	float		hdop;
	value		altitude;
	char		units;
	value		geoidal_separation;
} nmea_gga;

typedef enum RmcMode { ModeAutonomous, ModeDifferential, ModeEstimated } RmcMode;

typedef struct nmea_rmc {
	utc_time	time;
	char		status;
	float		latitude;
	float		longitude;
	float		speed;
	float		course;
	utc_date	date;
	value		magnetic_variation;
	RmcMode		mode;
} nmea_rmc;

bool parse_nmea(string sentence, nmea* x);

bool write_nmea(nmea* x, File file);

#endif /* NMEA_H_ */
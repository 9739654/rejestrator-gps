
#include <asf.h>
#include <stdlib.h>

#include "nmea.h"
#include "stdio.h"
#include "tools.h"


#define next_field(sentence, s, length, index)	\
s = &sentence[index];				\
length = indexof(',', s);			\
index += length + 1;

#define next_field_def(sentence, index)		\
string s = &sentence[index];			\
ubyte length = indexof(',', s);			\
index += length + 1;

bool parse_nmea(string sentence, nmea* x);
static bool parse_gga(string sentence, nmea* x);
static bool parse_rmc(string sentence, nmea* x);
static ubyte calcuate_checksum(string sentence);
static bool validate_sentence(string sentence);
static bool write_gga(nmea_gga* gga, File file);
static bool write_rmc(nmea_rmc* rmc, File file);

bool parse_nmea(string sentence, nmea* x) {
	if (sentence[0] != '$') {
		return false;
	}
	if (!validate_sentence(sentence)) {
		return false;
	}
	string kind = &sentence[3];
	if (eq(kind, "GGA", 3)) {
		//parse_gga(&sentence[7], x);
	} else if (eq(kind, "GSA", 3)) {
		
	} else if (eq(kind, "GSV", 3)) {
		
	} else if (eq(kind, "RMC", 3)) {
		//parse_rmc(&sentence[7], x);
	} else if (eq(kind, "VTG", 3)) {
		
	} else if (eq(kind, "ACK", 3)) {
		
	} else {
		
	}
	return true;
}

static bool parse_time(string sentence, utc_time* time, ubyte* index) {
	next_field_def(sentence, *index);
	if (length > 0) {
		time->hours =  10 * (s[0] - '0');
		time->hours +=  1 * (s[1] - '0');
		time->minutes =  10 * (s[2] - '0');
		time->minutes +=  1 * (s[3] - '0');
		time->seconds =  10 * (s[4] - '0');
		time->seconds +=  1 * (s[5] - '0');
		if (s[6] == '.') {
			*index += 4;
			time->milis =  100 * (s[7] - '0');
			time->milis +=  10 * (s[8] - '0');
			time->milis +=   1 * (s[9] - '0');
		}
	}
	return true;
}

static bool parse_float(string sentence, float* value, ubyte* index) {
	next_field_def(sentence, *index);
	
	if (length > 0) {
		*value = atof(s);
	}
	return true;
}

static void parse_char(string sentence, char* value, ubyte* index) {
	next_field_def(sentence, *index);
	
	if (length > 0) {
		*value = sentence[0];
	}
}

static nmea_gga gga;

static bool parse_gga(string sentence, nmea* x) {
	x->kind = GGA;
	x->data = &gga;
	
	ubyte index = 0;
	ubyte length;
	string s;
	// time
	next_field(sentence, s, length, index);
	parse_time(sentence, &gga.time, &index);
	
	// latitude
	next_field(sentence, s, length, index);
	if (length > 0) {
		uint32_t min;
		min  =       1 * (s[8] - '0');
		min +=      10 * (s[7] - '0');
		min +=     100 * (s[6] - '0');
		min +=    1000 * (s[5] - '0');
		min +=   10000 * (s[3] - '0');
		min +=  100000 * (s[2] - '0');
		gga.latitude  = min * 100.0 / 60.0;
		gga.latitude +=  100000 * (s[1] - '0');
		gga.latitude += 1000000 * (s[0] - '0');
	}
	
	// south/north
	next_field(sentence, s, length, index);
	if (length > 0) {
		if (s[0] == 'S') {
			gga.latitude = -gga.latitude;
		}
	}
	
	// longitude
	next_field(sentence, s, length, index);
	if (length > 0) {
		ubyte min;
		min  =       1 * (s[9] - '0');
		min +=      10 * (s[8] - '0');
		min +=     100 * (s[7] - '0');
		min +=    1000 * (s[6] - '0');
		min +=   10000 * (s[4] - '0');
		min +=  100000 * (s[3] - '0');
		gga.longitude  = min * 100.0 / 60.0;
		gga.longitude +=   100000 * (s[2] - '0');
		gga.longitude +=  1000000 * (s[1] - '0');
		gga.longitude += 10000000 * (s[0] - '0');
	}
	
	// west/east
	next_field(sentence, s, length, index);
	if (length > 0) {
		if (sentence[0] == 'W') {
			gga.longitude = -gga.longitude;
		}
	}
	
	// position fix indicator
	next_field(sentence, s, length, index);
	if (length > 0) {
		gga.fix = s[0] - '0';
	}
	
	// satellites used
	next_field(sentence, s, length, index);
	if (length == 1) {
		gga.sattelites = s[0] - '0';
	} else if (length == 2) {
		gga.sattelites  = 10 * s[0] - '0';
		gga.sattelites +=  1 * s[1] - '0';
	}
	
	// hdop
	parse_float(sentence, &gga.hdop, &index);
	
	// altitude
	parse_float(sentence, &gga.altitude.value, &index);
	
	next_field(sentence, s, length, index);
	if (length > 0) {
		gga.altitude.value = atof(s);
	}
	
	// altitude unit
	next_field(sentence, s, length, index);
	if (length > 0) {
		gga.altitude.unit = s[0];
	}

	// geoidal separation
	next_field(sentence, s, length, index);
	if (length > 0) {
		gga.geoidal_separation.value = atof(s);
	}
	
	// geoidal separation unit
	next_field(sentence, s, length, index);
	if (length > 0) {
		gga.geoidal_separation.unit = s[0];
	}
	
	// age of diff
	next_field(sentence, s, length, index);
	if (length > 0) {
		
	}
	
	// checksum
	s = &sentence[index];
	length = indexof(',', s);
	index += length + 1;
	if (sentence[0] != '*') {
		return false;
	}
	return true;
}

static nmea_rmc rmc;

static bool parse_rmc(string sentence, nmea* x) {
	x->kind = RMC;
	x->data = &rmc;
	
	ubyte length = 0;
	ubyte index;
	string s;
	
	// time
	parse_time(sentence, &rmc.time, &index);
	
	// status
	parse_char(sentence, &rmc.status, &index);
	
	// latitude
	next_field(sentence, s, length, index);
	if (length > 0) {
		uint32_t min;
		min  =       1 * (s[8] - '0');
		min +=      10 * (s[7] - '0');
		min +=     100 * (s[6] - '0');
		min +=    1000 * (s[5] - '0');
		min +=   10000 * (s[3] - '0');
		min +=  100000 * (s[2] - '0');
		rmc.latitude  = min * 100.0 / 60.0;
		rmc.latitude +=  100000 * (s[1] - '0');
		rmc.latitude += 1000000 * (s[0] - '0');
	}
	
	// north/south
	next_field(sentence, s, length, index);
	if (length > 0) {
		if (s[0] == 'S') {
			rmc.latitude *= -1;
		}
	}
	
	// longitude
	next_field(sentence, s, length, index);
	if (length > 0) {
		ubyte min;
		min  =       1 * (s[9] - '0');
		min +=      10 * (s[8] - '0');
		min +=     100 * (s[7] - '0');
		min +=    1000 * (s[6] - '0');
		min +=   10000 * (s[4] - '0');
		min +=  100000 * (s[3] - '0');
		rmc.latitude  = min * 100.0 / 60.0;
		rmc.latitude +=   100000 * (s[2] - '0');
		rmc.latitude +=  1000000 * (s[1] - '0');
		rmc.latitude += 10000000 * (s[0] - '0');
	}
	
	// east/west
	next_field(sentence, s, length, index);
	if (length > 0) {
		if (s[0] == 'W') {
			rmc.longitude *= -1;
		}
	}
	
	// speed
	parse_float(sentence, &rmc.speed, &index);
	
	// course
	parse_float(sentence, &rmc.course, &index);
	
	// date
	next_field(sentence, s, length, index);
	if (length > 0) {
		rmc.date.day  = 10 * (s[0] - '0');
		rmc.date.day +=  1 * (s[1] - '0');
		rmc.date.month  = 10 * (s[2] - '0');
		rmc.date.month +=  1 * (s[3] - '0');
		rmc.date.year  = 10 * (s[4] - '0');
		rmc.date.year += 10 * (s[5] - '0');
	}

	// magnetic variation
	parse_float(sentence, &rmc.magnetic_variation.value, &index);
	parse_char(sentence, &rmc.magnetic_variation.unit, &index);
	
	// mode
	next_field(sentence, s, length, index);
	if (length > 0) {
		switch(sentence[0]) {
			case 'A': rmc.mode = ModeAutonomous; break;
			case 'D': rmc.mode = ModeDifferential; break;
			case 'E': rmc.mode = ModeEstimated; break;
			default:
				return false;
		}
	}
	
	// checksum
	s = &sentence[index];
	length = indexof('\0', sentence);
	index += length + 1;
	if (sentence[0] != '*') {
		return false;
	}
	return true;
}

static ubyte calcuate_checksum(string sentence) {
	string checksum_start = &sentence[1];
	string checksum_end = &sentence[indexof('*', sentence)];
	
	ubyte checksum = 0x00;
	string pointer = checksum_start;
	while (pointer != checksum_end) {
		checksum ^= pointer[0];
		pointer = &pointer[1];
	}
	return checksum;
}

static bool validate_sentence(string sentence) {
	ubyte checksum = calcuate_checksum(sentence);
	ubyte test = 0;
	string pointer = &sentence[indexof('*', sentence)+1];
	char c = pointer[0];
	if (c >= '0' && c <= '9') {
		test = c - '0';
	} else if (c >= 'a' && c <= 'f') {
		test = c - 'a';
	} else {
		test = c - 'A';
	}
	test = test << 4;
	c = pointer[1];
	if (c >= '0' && c <= '9') {
		test += c - '0';
	} else if (c >= 'a' && c <= 'f') {
		test += c - 'a';
	} else {
		test += c - 'A';
	}
	return test == checksum;
}

bool write_nmea(nmea* x, File file) {
	switch(x->kind) {
		case GGA:
			return write_gga(x->data, file);
			break;
		case RMC:
			return write_rmc(x->data, file);
			break;
		default:
			return false;
	}
}

static bool write_gga(nmea_gga* gga, File file) {
	
}

static bool write_rmc(nmea_rmc* rmc, File file) {
	
}


#undef next_field

#include <asf.h>
#include "tools.h"

ubyte indexof(char c, string str) {
	for (uint i=0; i<254; i++) {
		if (str[i] == '\0') {
			return 255;
		}
		if (str[i] == c) {
			return i;
		}
	}
	return 255;
}

bool eq(string str1, string str2, uint count) {
	for (ubyte i=0; i<count; i++) {
		if (str1[i] != str2[i]) {
			return false;
		}
	}
	return true;
}
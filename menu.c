
#include "menu.h"

#include <stdio.h>
#include <util/delay.h>

#include "tools.h"

typedef enum MenuState { Main } MenuState;

static MenuState state = Main;

void btn_ok_onclick(void) {
	BLINK(B, 0, 1000, 0, 1);
	switch (state) {
		
		default:
			return;
	}
}

void btn_back_onclick(void) {
	printf("back");
}

void btn_left_onclick(void) {
	printf("left");
}

void btn_right_onclick(void) {
	printf("right");
}

void btn_down_onclick(void) {
	printf("down");
}

void btn_up_onclick(void) {
	printf("up");
}

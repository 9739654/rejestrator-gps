
#ifndef TOOLS_H_
#define TOOLS_H_

#include <asf.h>
#include "sd/ff.h"

#define SPI_DATA	SPDR
#define SPI_STATUS	SPSR

#define UART_DATA	UDR0

#ifndef false
#define false	1==2
#endif
#ifndef true
#define true	!false
#endif


/*
DDRx - wej�cie 0, wyj�cie 1
PINx - odczyt stanu
PORTx - jak wyj�cie, to warto�� logiczna. jak wej�cie to pullup
*/

#define B 0x03
#define C 0x06
#define D 0x09

#define PIN_SELECT_SD	1
#define PIN_SS		2
#define PIN_MOSI	3
#define PIN_MISO	4
#define PIN_SCK		5
#define PORT_SPI	B

typedef uint8_t		ubyte;
typedef uint16_t	ushort;
typedef unsigned int	uint;

typedef char*		string;

typedef uint8_t		port;

typedef FIL*		File;

#define null NULL

typedef struct pin {
	port port;
	ubyte pin;
} pin;
typedef pin* Pin;

typedef enum Direction {
	In = 0,
	Out
} Direction;

#define DIRECTION_IN	0
#define DIRECTION_OUT	1

#define PULLUP_NO	0
#define PULLUP_YES	1

#define STATE_READ_OFFSET	0
#define DIRECTION_SET_OFFSET	1
#define STATE_WRITE_OFFSET	2
#define PULLUP_OFFSET		2

#define PORT_DIR(port)		_SFR_IO8(port + DIRECTION_SET_OFFSET)
#define PORT_PULLUP(port)	_SFR_IO8(port + PULLUP_OFFSET)
#define PORT_READ(port)		_SFR_IO8(port + STATE_READ_OFFSET)
#define PORT_WRITE(port)	_SFR_IO8(port + STATE_WRITE_OFFSET)

#define r_assign(address, r)	address = (r)
#define r_set(address, r)	address |= (r)
#define r_clear(address, r)	address &= ~(r)
#define r_toggle(address, r)	address ^= (r)
#define r_get(address, r)	address & (r)

#define r_pin_set(address, pin)		r_set(address, (1<<pin))
#define r_pin_clear(address, pin)	r_clear(address, (1<<pin))
#define r_pin_toggle(address, pin)	r_toggle(address, (1<<pin))
#define r_pin_get(address, pin)		r_get(address, (1<<pin))

#define io_dir(port, pin, dir)	if (dir) { r_pin_set(PORT_DIR(port), pin); } else { r_pin_clear(PORT_DIR(port), pin); }

#define io_set(port, pin)	r_pin_set(PORT_WRITE(port), pin)
#define io_clear(port, pin)	r_pin_clear(PORT_WRITE(port), pin)
#define io_toggle(port, pin)	r_pin_toggle(PORT_WRITE(port), pin)
#define io_get(port, pin)	r_pin_get(PORT_READ(port), pin)
#define io_high(port, pin)	io_set(port, pin)
#define io_low(port, pin)	io_clear(port, pin)
#define io_on(port, pin)	io_set(port, pin)
#define io_off(port, pin)	io_clear(port, pin)

#define io_assign(port, pin, state)	if(state){ io_set(port, pin); }else{ io_clear(port,pin); }

#define io_pullup(port, pin, pull)	if(pull){ r_pin_set(PORT_PULLUP(port),pin); }else{ r_pin_clear(PORT_PULLUP(port),pin); }

#define BIT(n)		(1<<n)

#define PWM(port, pin, ms, fill) {	\
	for(int i=0; i<ms; i++) {	\
		io_high(port, pin);	\
		_delay_us(fill);	\
		io_low(port, pin);	\
		_delay_us(100-fill);	\
	}				\
}

#define BLINK(port, pin, ms_on, ms_off, count) {	\
	for (int i=0; i<count; i++) {			\
		io_high(port, pin);			\
		_delay_ms(ms_on);			\
		io_low(port, pin);			\
		_delay_ms(ms_off);			\
	}						\
}

ubyte indexof(char c, string str);
bool eq(string str1, string str2, uint count);

#endif /* TOOLS_H_ */